# Deadline24 - Vagrant

Project for setting up the development environment for GoHome.

## Set Up Vagrant

First, install [Vagrant](https://www.vagrantup.com/) and
[VirtualBox](https://www.virtualbox.org/). Then, run following command from the
project root:

```
$ vagrant up
```

It will download the ubuntu box and install all required dependencies like
nodejs, golang, vim, git, etc. If for any reason provisioning fails and has to
be retried, you can run:

```
$ vagrant provision
```

## Connect to Vagrant

### Linux / OSX

Now, connect to your newly created vagrant instance:

```
$ vagrant ssh
```

### Windows

Download
[putty](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html) and
[puttygen](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html).
Then, open `puttygen`, load the private key generated from
`.vagrant/machines/default/virtualbox/private_key` and save it as
`private_key.ppk`.

Now, configure putty:

* Connection -> SSH -> Auth, select `private_key.ppk` file
* Connection -> Data, set login as `vagrant`
* Window -> Selection, select `xterm`

## Fetch project

Once the system is provisioned, its time to fetch the project - we are not
doing it automatically because repository is private and it is required to
provide the creditentials to fetch it (either user / password or setup the
private key).

Configure git:

```
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
$ git config --global alias.st status
$ git config --global color.ui auto
```

There are two ways to fetch the repo:
- use https repo and always provide username and password
- create new private key and add it to your bitbucket account

To use https:

```
$ git clone https://bitbucket.org/doriath/deadline24-2017.git src/gohome
```

To use private key:

```
$ ssh-keygen
$ cat ~/.ssh/id_rsa.pub # Copy the output to bitbucket account
$ git clone git@bitbucket.org:doriath/deadline24.git
```
